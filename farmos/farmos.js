/*eslint no-undefined: "error"*/

const moment = require('moment');
const axios = require('axios');
const farmosApi = require('./farmosapi');
const templates = require('./templates');

async function getTillageCsv() {
    const p = encodeURIComponent('resources/tillage.csv');
    const url = `https://gitlab.com/api/v4/projects/11427416/repository/files/${p}/raw?ref=master`;
    const answer = await axios(url);
    const csv = answer.data;
    const lines = csv.split('\n');
    return lines
        .map(l => {
            const splitted = l.split('\t');
            if (splitted.length !== 3) {
                console.error('line malformated');
                console.error(l);
                return null;
            }
            return splitted;
        })
        .filter(args => args !== null);
}

const machines = {
    materials: 'farm_materials',
    crops: 'farm_crops',
    units: 'farm_quantity_units',
    soil: 'farm_soil_names',
    log: 'farm_log_categories'
};

async function deleteLogsWithUuid(api, token, uuid) {
    const logs = await api.log.get();
    const related = logs.filter(l => {
        if (!l.data) {
            return false;
        }

        const d = JSON.parse(l.data);
        if (!d || d.instanceId !== uuid) {
            return false;
        }

        return true;
    });

    for (let i = 0; i < related.length; i++) {
        try {
            await api.log.delete(related[i].id, token);
        } catch (error) {
            console.error(error);
        }
    }
}

async function createTerm(api, token, vocab, termsResponse, machineName, name) {
    if (!Object.values(machines).includes(machineName)) {
        throw Error(`Unable to find vocabulary for ${machineName}`);
    }

    const v = vocab.list.find(it => it.machine_name === machineName);
    if (!v) {
        throw Error(`unable to find vocabulary on farmos for ${machineName}`);
    }

    const materials = termsResponse.filter(it => it.vocabulary.id === v.vid);
    const material = materials.find(it => it.name === name);
    let id = material ? material.tid : '';
    if (!id) {
        try {
            const r = await api.term.send(name, machineName, token);
            ({ id } = r);
        } catch (error) {
            throw Error(
                `unable to create new term on farmos: ${name}, ${error.message}`
            );
        }
    }

    return id;
}

/**
 *
 * @param {string} url URL of the farmos instance
 * @param {string} username username of the farmos instance
 * @param {string} password password of the farmos instance
 * @param {string} token token provided, if null, authentication will take place in initialization
 * @param {object} config optional initial configuration for the session (fieldId and crop)
 */
module.exports = async function farmos(
    url,
    username,
    password,
    providedToken,
    {
        configFieldId = null,
        configCrop = null,
        configInstanceId = null,
        configClearEntrieswithInstanceId = false,
        configPlantingId = null
    } = {}
) {
    /**
     * initialization
     */

    const farmConfig = {
        cropId: null /* not yet defined, need ID */,
        cropName: configCrop,
        fieldId: configFieldId,
        fieldName: null,
        instanceId: configInstanceId,
        plantingId: configPlantingId
    };

    const farmapi = farmosApi(url, username, password);

    /**
     * here we are just checking if we can reach the farmos instance
     * and are actually expecting a 403
     */
    try {
        await axios(url);
    } catch (error) {
        if (error.response.status !== 403) {
            throw Error(
                `unable to reach farmos instance: ${url}, ${error.message}`
            );
        }
    }

    let token;
    try {
        token = providedToken || (await farmapi.authenticate());
    } catch (error) {
        throw Error(
            `unable to authenticate with farmos instance: ${url}, ${
                error.message
            }`
        );
    }

    let tillageTypes;

    try {
        tillageTypes = await getTillageCsv();
    } catch (error) {
        throw Error(`unable to fetch tillage types: ${error.message}`);
    }
    const vocab = await farmapi.vocabulary.get();

    let termsResponse;
    try {
        termsResponse = await farmapi.term.get({ token });
    } catch (error) {
        throw Error(`unable to fetch terms from farmos: ${error}`);
    }

    async function term(machineName, name) {
        return createTerm(
            farmapi,
            token,
            vocab,
            termsResponse,
            machineName,
            name
        );
    }

    function getTerms(vocabularyName) {
        return termsResponse.filter(t => t.vocabulary.id === vocabularyName);
    }

    const terms = {
        inches: await term(machines.units, 'inches'),
        pct: await term(machines.units, '%'),
        lbsPerAcre: await term(machines.units, 'lbs per acre'),
        tillage: await term(machines.log, 'Tillage'),
        irrigation: await term(machines.log, 'Water'),
        gallonsPerAcre: await term(machines.units, 'gal per acre'),
        soil: await term(machines.log, 'Soil'),
        planting: await term(machines.log, 'Plantings')
    };

    function assertConfiguration() {
        if (
            !farmConfig.cropId ||
            !farmConfig.fieldId ||
            !farmConfig.instanceId ||
            !farmConfig.cropName
        ) {
            throw Error(
                `error in configuration: ${JSON.stringify(farmConfig, null, 4)}`
            );
        }
    }

    async function configurePlanting(
        plantingId,
        instanceId,
        clearEntrieswithInstanceId = false
    ) {
        farmConfig.plantingId = plantingId;
        farmConfig.instanceId = instanceId;

        const assets = await farmapi.asset.get({
            type: 'planting'
        });

        const planting = assets.find(a => a.id === plantingId);
        if (!planting) {
            throw Error('unable to find planting on farmos');
        }

        if (!planting.location || !planting.location[0]) {
            throw Error('no location found for planting on farmos');
        }

        const { id } = planting.location[0];
        if (!id) {
            throw Error('no location ID found for planting on farmos');
        }

        farmConfig.fieldId = id;

        const field = termsResponse
            .filter(t => t.area_type === 'field' || t.area_type === 'bed')
            .find(f => f.tid === id);
        if (!field) {
            throw Error(`cannot resolve field with id ${id} on farmos`);
        }

        farmConfig.fieldName = field.name;

        const cropId = planting.crop[0].id;
        if (!cropId) {
            throw Error('unable to resolve cropId on farmos for planting');
        }

        const crop = termsResponse.find(t => t.tid === cropId);
        if (!crop) {
            throw Error(`unable to find crop with id ${cropId} on farmos`);
        }

        farmConfig.cropId = cropId;
        farmConfig.cropName = crop.name;
        assertConfiguration();

        if (!clearEntrieswithInstanceId) {
            return;
        }

        const r = await deleteLogsWithUuid(farmapi, token, instanceId);
        return r;
    }

    async function configureFieldAndCrop(
        fieldId,
        crop,
        instanceId,
        clearEntrieswithInstanceId = false
    ) {
        console.log(fieldId);
        farmConfig.instanceId = instanceId;
        farmConfig.cropName = crop;

        const field = termsResponse
            .filter(t => t.area_type === 'field' || t.area_type === 'bed')
            .find(f => f.tid === fieldId);

        if (!field) {
            throw Error(
                `unable to find field on farmos instance with id: ${fieldId}`
            );
        }

        farmConfig.fieldId = fieldId;
        farmConfig.fieldName = field.name;
        farmConfig.cropId = await term(machines.crops, crop);
        assertConfiguration();
        if (!clearEntrieswithInstanceId) {
            return;
        }

        const assets = await farmapi.asset.get({
            type: 'planting'
        });
        // console.log(assets);

        const target = assets.find(a => {
            if (!a.data) {
                return false;
            }

            const d = JSON.parse(a.data);
            if (!d || d.instanceId !== instanceId) {
                return false;
            }

            return true;
        });

        if (!target) {
            return;
        }

        const r = await deleteLogsWithUuid(farmapi, token, instanceId);

        try {
            await farmapi.asset.delete(target.id, token);
            console.log(`deleted asset with ID: ${target.id}`);
        } catch (error) {
            console.error(error);
        }
    }

    if (configFieldId && configInstanceId) {
        await configureFieldAndCrop(
            configFieldId,
            configCrop,
            configInstanceId,
            configClearEntrieswithInstanceId
        );
    } else if (configPlantingId) {
        await configurePlanting(
            configPlantingId,
            configInstanceId,
            configClearEntrieswithInstanceId
        );
    }

    return {
        configureFieldAndCrop,
        /**
         *
         * @param {string} type type of tillage
         * @param {string} date date in form 2019-11-24
         * @param {number} depth depth of tillage
         * @param {number} [assetId] id of planting asset, optional
         */
        async submitTillage(type, date, depth, assetId) {
            const timestamp = moment(date).unix();

            const tillageData = templates.tillage(
                timestamp,
                type,
                depth,
                farmConfig.fieldId,
                tillageTypes,
                terms.tillage,
                terms.inches,
                assetId,
                farmConfig.instanceId
            );

            let tillage;

            try {
                tillage = await farmapi.log.send(tillageData, token);
            } catch (error) {
                throw Error(
                    `unable to submit tillage to farmos: ${error.message}`
                );
            }

            return tillage;
        },
        /**
         *
         * @param {string} material mulch material
         * @param {string} date date in format 2019-11-26
         * @param {number} depth depth in inches
         * @param {string} method method for mulching
         * @param {string} source source for mulching
         * @param {number} [assetId] assetId of planting, optional
         */
        async submitMulch(material, date, depth, method, source, assetId) {
            const timestamp = moment(date).unix();
            const materialId = await term(machines.materials, material);

            const data = templates.input(
                timestamp,
                null,
                `Mulch for ${farmConfig.cropName} planted on ${date}`,
                materialId,
                farmConfig.fieldId,
                method,
                source,
                'mulching',
                assetId,
                depth
                    ? [
                        {
                            measure: 'length',
                            value: depth,
                            unit: {
                                id: terms.inches
                            },
                            label: 'Depth'
                        }
                    ]
                    : [],
                {
                    instanceId: farmConfig.instanceId
                }
            );
            const log = await farmapi.log.send(data, token);
            return log;
        },

        /**
         * @param {string} date date in format 2019-12-25
         */
        async submitPlanting(plantingDate) {
            const plantingData = templates.planting(
                `${moment(plantingDate).format('YYYY-MM-DD')} ${
                    farmConfig.fieldName
                } ${farmConfig.cropName}`,
                farmConfig.cropName,
                farmConfig.cropId,
                farmConfig.instanceId
            );

            let planting;
            try {
                console.log(plantingData);
                planting = await farmapi.asset.send(plantingData, '', token);
            } catch (error) {
                throw Error(
                    `unable to create planting on farmos: ${error.message}`
                );
            }

            const assetId = planting.id;
            if (!assetId) {
                throw Error(
                    `did not receive asset id from farmos: ${JSON.stringify(
                        planting,
                        null,
                        4
                    )}`
                );
            }

            return assetId;
        },
        async submitSeeding(
            timestamp,
            assetId,
            movement,
            source,
            quantities = []
        ) {
            for (let i = 0; i < quantities.length; i++) {
                const q = quantities[i];

                if (q.unit) {
                    const u = q.unit;
                    q.unit = {
                        id: await term(machines.units, u)
                    };
                }
            }

            const seedingData = templates.seeding(
                farmConfig.cropName,
                assetId,
                movement ? farmConfig.fieldId : null,
                timestamp,
                farmConfig.instanceId,
                source
            );

            if (quantities) {
                Object.assign(seedingData, { quantity: quantities });
            }

            let seeding;
            try {
                seeding = await farmapi.log.send(seedingData, token);
            } catch (error) {
                throw Error(
                    `unable to create movement on farmos: ${error.message}`
                );
            }
            return seeding;
        },
        async submitIrrigationSource(irrigation) {},
        async submitIrrigation(date, rate, type) {
            const timestamp = moment(date).unix();
            const waterId = await term(machines.materials, 'Water');
            const irrigationData = templates.input(
                timestamp,
                null,
                `Irrigation for ${farmConfig.cropName} on ${date}`,
                waterId,
                farmConfig.fieldId,
                type,
                null,
                'irrigation',
                farmConfig.plantingId,
                [
                    {
                        measure: 'count',
                        value: rate,
                        Label: 'irrigations in the past week'
                    }
                ],
                {
                    instanceId: farmConfig.instanceId
                },
                terms.irrigation
            );
            const log = await farmapi.log.send(irrigationData, token);
            return log;
        },
        async submitLime(limeRate, date, assetId) {
            const materialId = await term(machines.materials, 'lime');
            const timestamp = moment(date).unix();
            const limeData = templates.input(
                timestamp,
                null,
                `Lime for ${farmConfig.cropName} planted on ${date}`,
                materialId,
                farmConfig.fieldId,
                null,
                null,
                'lime',
                assetId,
                [
                    {
                        measure: 'ratio',
                        value: limeRate,
                        unit: {
                            id: terms.lbsPerAcre
                        },
                        label: 'Rate'
                    }
                ],
                {
                    instanceId: farmConfig.instanceId
                }
            );
            console.log(limeData);
            const log = await farmapi.log.send(limeData, token);
            return log;
        },
        async submitSimpleActivity(name, pAssetId, timestamp) {
            let assetId = pAssetId;
            if (!assetId) {
                assetId = farmConfig.plantingId;
            }

            const activityData = templates.activity(
                timestamp,
                { instanceId: farmConfig.instanceId },
                name,
                farmConfig.fieldId,
                assetId,
                terms.soil
            );

            let r;
            try {
                r = await farmapi.log.send(activityData, token);
            } catch (error) {
                throw Error(
                    `cannot send log for amendment ${name}, ${error.message}`
                );
            }
            return r;
        },
        async submitSimpleAmendment(name, pAssetId, timestamp) {
            let assetId = pAssetId;
            if (!assetId) {
                assetId = farmConfig.plantingId;
            }

            const materialId = await term(machines.materials, name);
            const inputData = templates.input(
                timestamp,
                null,
                `${name} for ${farmConfig.cropName}`,
                materialId,
                farmConfig.fieldId,
                null,
                null,
                null,
                assetId,
                null,
                {
                    instanceId: farmConfig.instanceId,
                    name
                }
            );

            let r;
            try {
                r = await farmapi.log.send(inputData, token);
            } catch (error) {
                throw Error(
                    `cannot send log for amendment ${name}, ${error.message}`
                );
            }
            return r;
        },
        async submitAmendment(
            name,
            pAssetId,
            timestamp,
            method,
            type,
            nutrients,
            n,
            p,
            k,
            nutrientsTrace
        ) {
            let assetId = pAssetId;
            if (!assetId) {
                assetId = farmConfig.plantingId;
            }
            const quanitities = [];
            if (n) {
                quanitities.push({
                    measure: 'ratio',
                    value: n,
                    unit: {
                        id: terms.lbsPerAcre
                    },
                    label: 'N'
                });
            }

            if (p) {
                quanitities.push({
                    measure: 'ratio',
                    value: p,
                    unit: {
                        id: terms.lbsPerAcre
                    },
                    label: 'P'
                });
            }

            if (k) {
                quanitities.push({
                    measure: 'ratio',
                    value: k,
                    unit: {
                        id: terms.lbsPerAcre
                    },
                    label: 'K'
                });
            }

            const materialId = await term(machines.materials, type);

            const inputData = templates.input(
                timestamp,
                null,
                `${name} for ${farmConfig.cropName}, ${nutrients}`,
                materialId,
                farmConfig.fieldId,
                method,
                null,
                null,
                assetId,
                quanitities,
                {
                    instanceId: farmConfig.instanceId,
                    name,
                    nutrients,
                    nutrientsTrace
                }
            );

            let r;
            try {
                r = await farmapi.log.send(inputData, token);
            } catch (error) {
                throw Error(
                    `cannot send log for amendment ${name}, ${error.message}`
                );
            }
            return r;
        },
        async submitWeeklyAmendment(
            name,
            assetId,
            timestamp,
            nutrients,
            n,
            p,
            k,
            nutrientsTrace
        ) {},
        async submitHistory(use, cashCrop1, cashCrop2, coverCrop, date) {},
        async submitTransplanting(
            pottingSoilBrand,
            seedlingTrayType,
            cellNumber,
            lighting,
            climate,
            seedlingTreatment,
            treatmentDate,
            treatmentName,
            timestamp,
            assetId
        ) {
            if (pottingSoilBrand) {
                const materialId = await term(
                    machines.materials,
                    'potting soil'
                );
                const soilInputData = templates.input(
                    timestamp,
                    null,
                    `Potting Soil ${pottingSoilBrand} on ${moment
                        .unix(timestamp)
                        .format('YYYY-MM-DD')} for ${farmConfig.cropName}`,
                    materialId,
                    null,
                    null,
                    pottingSoilBrand,
                    '',
                    assetId,
                    null,
                    {
                        instanceId: farmConfig.instanceId
                    }
                );
                await farmapi.log.send(soilInputData, token);
            }

            if (
                seedlingTreatment &&
                seedlingTreatment.toLowerCase() !== 'none'
            ) {
                const seedlingTimestamp = moment(treatmentDate).unix();
                const materialId = await term(
                    machines.materials,
                    'seedling treatment'
                );
                const seedlingInputData = templates.input(
                    seedlingTimestamp,
                    null,
                    `Seedling Treatment: (${seedlingTreatment}) on ${treatmentDate} for ${
                        farmConfig.cropName
                    }`,
                    materialId,
                    null,
                    null,
                    treatmentName,
                    null,
                    assetId,
                    null,
                    {
                        instanceId: farmConfig.instanceId,
                        seedlingTrayType,
                        cellNumber,
                        lighting,
                        climate
                    }
                );
                await farmapi.log.send(seedlingInputData, token);
            }

            const transplantingData = templates.transplanting(
                farmConfig.fieldId,
                `Transplanting for ${farmConfig.cropName}`,
                assetId,
                farmConfig.instanceId,
                timestamp
            );

            let transplanting;
            try {
                transplanting = await farmapi.log.send(
                    transplantingData,
                    token
                );
            } catch (error) {
                throw Error(
                    `unable to run transplanting on farmos ${error.message}`
                );
            }
            return transplanting;
        },
        async submitDiseasePressure(timestamp, pAssetId, pressure) {
            let assetId = pAssetId;
            if (!assetId) {
                assetId = farmConfig.plantingId;
            }

            const quantity = [
                {
                    measure: 'rating',
                    value: pressure,
                    Label: 'pest / disease pressure'
                }
            ];

            const pressureData = templates.observation(
                timestamp,
                null,
                'Pest / Disease Pressure',
                farmConfig.fieldId,
                assetId,
                quantity,
                {
                    instanceId: farmConfig.instanceId
                },
                terms.planting
            );

            const observation = await farmapi.log.send(pressureData, token);
            return observation;
        },
        async submitHerbicide(timestamp, pAssetId, type, rate) {
            let assetId = pAssetId;
            if (!assetId) {
                assetId = farmConfig.plantingId;
            }

            const materialId = await term(machines.materials, type);

            const inputData = templates.input(
                timestamp,
                null,
                `Herbicide for ${farmConfig.cropName}`,
                materialId,
                null,
                null,
                null,
                null,
                assetId,
                [
                    {
                        measure: 'rate',
                        value: rate,
                        unit: {
                            id: terms.gallonsPerAcre
                        },
                        label: type
                    }
                ],
                {
                    instanceId: farmConfig.instanceId,
                    type
                },
                terms.planting
            );

            let r;
            try {
                r = await farmapi.log.send(inputData, token);
            } catch (error) {
                throw Error(
                    `cannot send log for amendment ${name}, ${error.message}`
                );
            }
            return r;
        },
        async submitPestDiseaseControl(
            timestamp,
            name,
            inputTypes,
            pruning,
            rate,
            pAssetId,
            data
        ) {
            Object.assign(data, { instanceId: farmConfig.instanceId });
            let assetId = pAssetId;
            if (!assetId) {
                assetId = farmConfig.plantingId;
            }

            let logData;

            const types = await Promise.all(
                inputTypes.map(t => term(machines.materials, t))
            );

            console.log(types);

            // material: [{ id: materialId }]
            const materials = types.map(t => ({ id: t }));
            console.log(materials);
            let r;
            if (materials) {
                // if only pruning is selected, skip this part, no materials added then
                logData = templates.input(
                    timestamp,
                    null,
                    name,
                    null,
                    farmConfig.fieldId,
                    null,
                    null,
                    null,
                    assetId,
                    rate
                        ? [
                            {
                                measure: 'rate',
                                value: rate,
                                unit: {
                                    id: terms.gallonsPerAcre
                                },
                                label: 'Spray'
                            }
                        ]
                        : null,
                    data,
                    terms.planting
                );
                Object.assign(logData, { material: materials });
                r = await farmapi.log.send(logData, token);
            }

            if (pruning) {
                pruningLog = templates.activity(
                    timestamp,
                    data,
                    'Hand-picking/pruning',
                    null,
                    assetId,
                    terms.planting
                );
                const resp = await farmapi.log.send(pruningLog, token);
                if (!r) {
                    r = resp;
                }
            }
            return r;
        },
        async submitInput(
            name,
            assetId,
            fieldId,
            date,
            cat,
            method,
            source,
            purpose,
            mats,
            quantities = []
        ) {
            const data = { instanceId: farmConfig.instanceId };
            const category = cat ? await term(machines.log, cat) : null;
            const types = mats
                ? await Promise.all(mats.map(t => term(machines.materials, t)))
                : [];

            const ids = await Promise.all(
                quantities.map(q => term(machines.units, q.unit))
            );

            // material: [{ id: materialId }]
            const materials = types.map(t => ({ id: t }));

            const logData = templates.input(
                date.unix(),
                null,
                name,
                null,
                fieldId,
                method,
                source,
                purpose,
                assetId,
                quantities.map((q, idx) => ({
                    label: q.label,
                    measure: q.measure,
                    value: q.value,
                    unit: {
                        id: ids[idx]
                    }
                })),
                data,
                category
            );

            if (materials) {
                Object.assign(logData, { material: materials });
            }

            console.log(logData);

            return farmapi.log.send(logData, token);
        },
        async submitActivity(
            name,
            assetId,
            fieldId,
            date,
            cat,
            quantities = [],
            data = {},
            harvest
        ) {
            Object.assign(data, { instanceId: farmConfig.instanceId });
            const category = await term(machines.log, cat);
            for (let i = 0; i < quantities.length; i++) {
                const q = quantities[i];

                if (q.unit) {
                    const u = q.unit;
                    q.unit = {
                        id: await term(machines.units, u)
                    };
                }

                if (q.tillageType) {
                    const stir = tillageTypes.find(c => c[0] === q.tillageType);
                    if(stir){
                        const [, name, stirValue] = stir;

                        quantities.push({
                            measure: 'rating',
                            value: stirValue,
                            label: 'STIR ' + name + ' / ' + q.label
                        });
                    }
                    
                }
            }

            console.log(quantities);

            const logData = templates.activity(
                date.unix(),
                data,
                name,
                fieldId,
                assetId,
                category,
                quantities
                    ? quantities.map(q => ({
                        label: q.label,
                        measure: q.measure,
                        value: q.value,
                        unit: q.unit || null
                    }))
                    : []
            );

            if (harvest) {
                Object.assign(logData, { type: 'farm_harvest' });
            }

            return farmapi.log.send(logData, token);
        },
        async submitObservation(
            name,
            asstId,
            fieldId,
            date,
            cat,
            quantities = [],
            data = {}
        ) {
            Object.assign(data, { instanceId: farmConfig.instanceId });
            const category = await term(machines.log, cat);

            const ids = await Promise.all(
                quantities.map(q => term(machines.units, q.unit))
            );

            const logData = templates.observation(
                date.unix(),
                null,
                name,
                fieldId,
                asstId,
                quantities.map((q, idx) => ({
                    label: q.label,
                    measure: q.measure,
                    value: q.value,
                    unit: {
                        id: ids[idx]
                    }
                })),
                data,
                category
            );
            return farmapi.log.send(logData, token);
        },
        async submitPruning(timestamp, types, typeArray, pAssetId) {
            const data = {
                instanceId: farmConfig.instanceId,
                pruning: typeArray
            };
            const name = `Pruning ${moment
                .unix(timestamp)
                .format('YYYY-MM-DD')} - ${farmConfig.cropName} - ${types}`;
            let assetId = pAssetId;
            if (!assetId) {
                assetId = farmConfig.plantingId;
            }

            const logData = templates.activity(
                timestamp,
                data,
                name,
                null,
                assetId,
                terms.planting
            );
            return farmapi.log.send(logData, token);
        },
        async submitHarvest(timestamp, harvest, pAssetId, complete = false) {
            const data = { instanceId: farmConfig.instanceId };
            const name = `Harvest ${moment
                .unix(timestamp)
                .format('YYYY-MM-DD')} - ${farmConfig.cropName}`;
            let assetId = pAssetId;
            if (!assetId) {
                assetId = farmConfig.plantingId;
            }

            const unitIds = await Promise.all([
                term(machines.units, harvest.weight.unit),
                term(machines.units, harvest.area.unit),
                term(machines.units, harvest.density.unit),
                harvest.plantDensity
                    ? term(machines.units, harvest.plantDensity.unit)
                    : null
            ]);

            const quantities = [
                {
                    measure: 'weight',
                    value: harvest.weight.value.toFixed(4),
                    unit: {
                        id: unitIds[0]
                    },
                    label: 'Weight of Harvest'
                },
                {
                    measure: 'area',
                    value: harvest.area.value.toFixed(4),
                    unit: {
                        id: unitIds[1]
                    },
                    label: 'Harvest Area'
                },
                {
                    measure: 'rate',
                    value: harvest.density.value.toFixed(4),
                    unit: {
                        id: unitIds[2]
                    },
                    label: 'Harvest Yield Rate'
                }
            ];

            if (harvest.plantDensity) {
                quantities.push({
                    measure: harvest.plantDensity.type,
                    value: harvest.plantDensity.value.toFixed(4),
                    unit: {
                        id: unitIds[3]
                    },
                    label: harvest.plantDensity.name
                });
            }

            const logData = templates.harvest(
                timestamp,
                data,
                name,
                farmConfig.fieldId,
                farmConfig.plantingId,
                quantities
            );

            const archivedTimeStamp = Math.floor(new Date().getTime() / 1000);

            if (complete) {
                const r = await farmapi.asset.alter(
                    {
                        archived: archivedTimeStamp
                    },
                    assetId,
                    token
                );
                console.log('alter:');
                console.log(r);
            }
            return farmapi.log.send(logData, token);
        }
    };
};

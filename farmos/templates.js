function log({
    timestamp,
    logName,
    type,
    fieldId,
    assetId,
    note: { content, format = 'plain_text' } = {},
    data,
    done = 1,
    movementFieldId,
    logCategory
} = {}) {
    if (!logName) {
        throw Error('name missing');
    }
    if (!timestamp) {
        throw Error('timestamp missing');
    }

    const obj = {
        name: logName,
        timestamp,
        done
    };

    Object.assign(obj, fieldId ? { area: [{ id: fieldId }] } : undefined);
    Object.assign(obj, assetId ? { asset: [{ id: assetId }] } : undefined);
    Object.assign(
        obj,
        logCategory ? { log_category: [{ id: logCategory }] } : undefined
    );
    // Object.assign(obj, data ? { data } : undefined);
    Object.assign(
        obj,
        movementFieldId
            ? { movement: { area: [{ id: movementFieldId }] } }
            : undefined
    );

    Object.assign(
        obj,
        content ? { notes: { value: content, format } } : undefined
    );
    Object.assign(obj, type ? { type } : undefined);
    Object.assign(obj, data ? { data: JSON.stringify(data) } : undefined);

    return obj;
}

function taxonomyTerm(vocabularyId, name, description) {
    return {
        name,
        description,
        vocabulary: {
            id: vocabularyId
        }
    };
}

function input(
    timestamp,
    note,
    name,
    materialId,
    fieldId,
    method,
    source,
    purpose,
    assetId,
    quantities,
    data,
    logCategory
) {
    const r = log({
        timestamp,
        logName: name,
        type: 'farm_input',
        fieldId,
        assetId,
        note: {
            content: note,
            format: 'plain_text'
        },
        done: 1,
        data,
        logCategory
    });

    Object.assign(
        r,
        materialId ? { material: [{ id: materialId }] } : undefined
    );
    Object.assign(r, quantities ? { quantity: quantities } : undefined);
    Object.assign(r, purpose ? { input_purpose: purpose } : undefined);
    Object.assign(r, method ? { input_method: method } : undefined);
    Object.assign(r, source ? { input_source: source } : undefined);

    return r;
}

function observation(
    timestamp,
    note,
    name,
    fieldId,
    assetId,
    quantities,
    data,
    logCategory
) {
    const r = log({
        timestamp,
        logName: name,
        type: 'farm_observation',
        fieldId,
        assetId,
        note: {
            content: note,
            format: 'plain_text'
        },
        done: 1,
        data,
        logCategory
    });

    Object.assign(r, quantities ? { quantity: quantities } : undefined);
    return r;
}

function transplanting(fieldId, name, assetId, instanceId, timestamp) {
    const r = log({
        timestamp,
        logName: name,
        type: 'farm_transplanting',
        done: 1,
        assetId,
        movementFieldId: fieldId,
        data: {
            instanceId
        }
    });

    return r;
}

function planting(name, crop, cropId, instanceId) {
    return {
        name,
        type: 'planting',
        crop: [
            {
                id: cropId
            }
        ],
        data: JSON.stringify({
            instanceId
        })
    };
}

function seeding(cropName, assetId, fieldId, timestamp, instanceId, source) {
    const r = log({
        logName: cropName,
        movementFieldId: fieldId,
        type: 'farm_seeding',
        assetId,
        timestamp,
        done: 1,
        data: {
            instanceId
        }
    });

    if (source) {
        Object.assign(r, { seed_source: source });
    }
    return r;
}

function activity(
    timestamp,
    data,
    name,
    fieldId,
    assetId,
    logCategory,
    quantities
) {
    const r = log({
        timestamp,
        logName: name,
        type: 'farm_activity',
        done: 1,
        fieldId,
        assetId,
        logCategory,
        data
    });
    if (quantities) {
        Object.assign(r, quantities ? { quantity: quantities } : undefined);
    }
    return r;
}

function harvest(timestamp, data, name, fieldId, assetId, quantities) {
    const r = log({
        timestamp,
        logName: name,
        type: 'farm_harvest',
        done: 1,
        fieldId,
        assetId,
        data
    });
    Object.assign(r, quantities ? { quantity: quantities } : undefined);
    return r;
}

function tillage(
    timestamp,
    type,
    depth,
    fieldId,
    tillageCsv,
    logCategory,
    inchesId,
    assetId,
    instanceId
) {
    const stir = tillageCsv.find(c => c[0] === type);

    let name = type,
        stirValue = null;
    if (stir) {
        [, name, stirValue] = stir;
    }

    const r = log({
        timestamp,
        logName: name,
        type: 'farm_activity',
        done: 1,
        fieldId,
        assetId,
        logCategory,
        data: [
            {
                instanceId
            }
        ]
    });

    const q = [
        {
            measure: 'length',
            value: depth,
            unit: {
                id: inchesId
            },
            label: 'Depth'
        }
    ];

    if (stirValue) {
        q.push({
            measure: 'rating',
            value: stirValue,
            label: 'STIR'
        });
    }

    Object.assign(r, {
        quantity: q
    });

    return r;
}

module.exports = {
    taxonomyTerm,
    input,
    observation,
    transplanting,
    planting,
    seeding,
    activity,
    tillage,
    harvest
};

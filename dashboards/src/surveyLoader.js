export default function requestFetchCsv(ids, sharecodeIds = {}) {
    /**
     * @param {Object} ids - Object with keys of survey ids to be fetched. Value for each survey key is an object containing key 'id' with value of survey id
     * @param {Object} [sharecodeIds] - optional object with keys of survey ids to be fetched with sharecode. Value for each survey key is an object containing key 'id' with value of survey id
     * @return {Promise} a promise that resolves to the response payload from the parent container: an object with keys of each survey id and values as the parsed survey data for each of survey
     * @example
     *
     *  const ids = {
     *      survey_1: { id: 'survey_1'},
     *      survey_2: { id: 'survey_2' },
     *  };
     *  const sharecodeIds = { survey_3: { id: 'survey_3' } };
     *  const response = await requestFetchCsv(ids, sharecodeIds); // => { survey_1: {...}, survey_2: {...}, survey_3: {...}, };
     * 
     */
    let origin;

    try {
        origin = window.location.ancestorOrigins[0];
    } catch (error) {
        origin = 'https://app.our-sci.net';
    }

    window.parent.postMessage(
        {
            type: 'REQUEST_FETCH_CSV',
            payload: { ids, sharecodeIds },
        },
        origin
    );

    return new Promise((resolve, reject) => {
        window.addEventListener('message', event => {
            if (
                !event.data ||
                !event.data.type ||
                event.data.type !== 'RETURN_FETCH_CSV'
            ) {
                return;
            }
            if (
                event.data &&
                event.data.type &&
                event.data.type === 'RETURN_FETCH_CSV'
            ) {
                resolve(event.data.payload);
            }
        });
    });
}

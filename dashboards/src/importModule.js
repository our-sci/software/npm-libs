export function importModule(url) {
    return new Promise((resolve, reject) => {
        const script = document.createElement('script');
        const destructor = () => {
            script.onerror = null;
            script.onload = null;
        };
        script.onerror = () => {
            reject(new Error(`Failed to import: ${url}`));
            destructor();
        };
        script.onload = () => {
            resolve();
            destructor();
        };

        script.src = url;
        document.head.appendChild(script);
    });
}

export default importModule;

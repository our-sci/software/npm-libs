export = dashboard;
export as namespace dashboard;

declare const dashboard: {
sayHello(): void;
info(title: string, description: string, icon: string): void;
progressBox(
    title: any,
    content: any,
    progress: any,
    progressText: any,
    icon: any
): void;
actionBox(
    title: string,
    content: string,
    action: string,
    icon: string,
    actionIcon: string,
    callback: () => void
): void;
fetchSurveys(surveys: any): Promise<any>;
style(primary: string, secondary: string, accent: string): void;
debug(title: string, data: any): void;
lineChart(title: string, x: any, values: any, axis: any): void;
surveyUtils: {
surveyToCSV(survey: any): void;
mergeSurveys(surveysParam: any): any;
joinSurveyOn(
    baseSurveyParam: 0,
    baseSurveyJoinField: any,
    surveyToJoin: any,
    surveyToJoinField: any,
    prefix: any
): 0;
};
};

const path = require("path");
const webpack = require("webpack");
module.exports.config = {
    entry: {
      app: ["@babel/polyfill", "./src/dashboard.js"]
      // parent: ['babel-polyfill', '../src/lib/parent.js'],
    },
    mode: "production",
    output: {
      path: path.resolve(__dirname, "..", "dist"),
      filename: "bundle.js",
      library: "",
      libraryTarget: "window"
    },
    externals: {
      plotly: "Plotly",
      lodash: "_",
      papaparse: "Papa",
      jquery: '$'
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"]
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          exclude: /node_modules/
        },
        {
          test: /\.css$/,
          use: [
            "style-loader",
            {
              loader: "css-loader"
            }
          ]
        },
        {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          use: ["url-loader?limit=10000&mimetype=application/font-woff"]
        },
        {
          test: /\.(ttf|eot|svg|png|jpg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          use: ["file-loader"]
        },
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env"]
            }
          }
        },
        {
          test: /\.csv$/,
          use: "raw-loader"
        },
        {
          test: /\.html$/,
          use: "raw-loader"
        },
        { test: /\.handlebars$/, loader: "handlebars-loader" }
      ]
    },
    plugins: [new webpack.NamedModulesPlugin()]
  };
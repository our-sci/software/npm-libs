const path = require("path");
const webpack = require("webpack");
const server = require("webpack-dev-server");
const fs = require("fs");
const {config} = require("./config");
const compiler = webpack(config);

compiler.run((err, stats) => {
  if (err) {
    console.error(err.stack || err);
    if (err.details) {
      console.error(err.details);
      throw err;
    }
    return;
  }

  const info = stats.toJson();

  let finalize = true;
  if (stats.hasErrors()) {
    console.log("\x1b[31m");
    info.errors.forEach(element => {
      console.error(element);
    });
    console.log("\x1b[0m");
    finalize = false;
  }

  if (stats.hasWarnings()) {
    console.log("\x1b[33m");
    info.warnings.forEach(element => {
      console.warn(element);
    });
    console.log("\x1b[0m");
  }

  if (!finalize) {
    return;
  }

  const globals = fs.readFileSync(
    path.resolve(__dirname, "..", "src", "dashboard.d.ts"),
    "utf-8"
  );
  const out = globals.replace("export default dashboard;", "");
  fs.writeFileSync(path.resolve(__dirname, "..", "dist", "globals.d.ts"), out);
});

/* global dashboard */
/* eslint-disable no-console */
const version = '1';

const CDN_BASE = 'https://cdn.jsdelivr.net/npm';

/**
 * Style dependencies
 */
const links = [
    'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
    'https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css',
    'https://fonts.googleapis.com/css?family=Open+Sans',
    'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css',
    'https://api.mapbox.com/mapbox-gl-js/v0.50.0/mapbox-gl.css',
    'https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css'
];
/**
 * Dependencies for the dashboard library, add oyur own here
 */
const dependencies = [
    'https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js',
    'https://cdn.plot.ly/plotly-latest.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/PapaParse/4.6.1/papaparse.min.js',
    'https://d3js.org/d3.v5.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js',
    'https://api.mapbox.com/mapbox-gl-js/v0.50.0/mapbox-gl.js',
    'https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js'
];

function createPromises(additionalExternals) {
    const ret = [];
    ret.push(links.map(l => importStyle(l)));
    ret.push(dependencies.map(d => importModule(d)));
    if (additionalExternals) {
        ret.push(additionalExternals.map(d => importModule(d)));
    }
    return ret;
}

export function importModule(url) {
    return new Promise((resolve, reject) => {
        const script = document.createElement('script');
        const destructor = () => {
            script.onerror = null;
            script.onload = null;
        };
        script.onerror = () => {
            reject(new Error(`Failed to import: ${url}`));
            destructor();
        };
        script.onload = () => {
            resolve();
            destructor();
        };

        script.src = url;
        document.head.appendChild(script);
    });
}

function importStyle(url) {
    const link = document.createElement('link');
    link.href = url;
    link.rel = 'stylesheet';
    document.head.appendChild(link);
}

export function init({
    surveys = [],
    sharecodeSurveys = [],
    style = {
        primary: '#de5555',
        secondary: '#e88787',
        accent: '#287dd5'
    },
    render = () => {
        console.log('default render');
        dashboard.info('Something went wrong', 'Dashboard not implemented');
    },
    externals,
} = {}) {
    // params is an object with a key for each survey id
    /**
     * params is an object with a key for each survey id, the value of which is an object containing the key id with value 
     * e.g.
     * { 
     *  survey_1: { id: 'survey_1' },
     *  survey_2: { id: 'survey_1' },
     *  ...
     *  survey_n: { id: 'survey_n' },
     * }
     * 
     */
    const params = {};
    Object.keys(surveys).forEach(k => {
        params[k] = { id: surveys[k] };
    });

    const sharecodeParams = {};
    Object.keys(sharecodeSurveys).forEach(k => {
        sharecodeParams[k] = { id: sharecodeSurveys[k] };
    });


    if (!Array.isArray(externals)) {
        console.error('externals must be an array!');
        externals = [];
    }

    (async () => {
        await importModule(
            'https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js'
        );
        await importModule(
            'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'
        );
        await Promise.all(createPromises(externals));

        const onlineURL = 'https://our-sci.gitlab.io/software/npm-libs/v1/dashboard.js';
        const devURL = 'http://localhost:7654/bundle.js';

        if (location.host === '127.0.0.1:5500') {
            try {
                await importModule(devURL);
            } catch (error) {
                await importModule(onlineURL);
            }
        } else {
            await importModule(onlineURL);
        }

        dashboard.style(style.primary, style.secondary, style.accent);
        dashboard.showLoader();

        const res = await dashboard.fetchSurveys(params, sharecodeParams);
        dashboard.hideLoader();

        render(res);
    })();

    function importModule(url) {
        return new Promise((resolve, reject) => {
            const script = document.createElement('script');
            const destructor = () => {
                script.onerror = null;
                script.onload = null;
            };
            script.onerror = () => {
                reject(new Error(`Failed to import: ${url}`));
                destructor();
            };
            script.onload = () => {
                resolve();
                destructor();
            };

            script.src = url;
            document.head.appendChild(script);
        });
    }
}

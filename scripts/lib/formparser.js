/* eslint-disable no-continue */
/* eslint-disable no-plusplus */

const app = require('./app');

function tillageParser() {
    const ret = [];
    for (let i = 1; i < 4; i++) {
        const type = app.getAnswer(`tillage/type_${i}`);
        if (!type) {
            continue;
        }

        const date = app.getAnswer(`tillage/date_${i}`);
        if (!date) {
            throw new Error(`empty date of tillage ${i}`);
        }

        const depth = app.getAnswer(`tillage/depth_${i}`);
        if (!depth) {
            throw new Error(`empty depth of tillage ${i}`);
        }

        const tillage = {
            type,
            date,
            depth
        };
        ret.push(tillage);
    }

    return ret;
}

function amendmentParser() {
    const ret = [];
    for (let i = 1; i < 4; i++) {
        const name = app.getAnswer(`amendments/name_${i}`);
        if (!name) {
            continue;
        }

        const type = app.getAnswer(`amendments/amendment_type_${1}`);

        const date = app.getAnswer(`amendments/amendment_date_${i}`);
        if (!date) {
            throw new Error(`empty date of amendment ${i}`);
        }

        const method = app.getAnswer(`amendments/method_${i}`);
        if (!method) {
            throw new Error(`empty method of amendment ${i}`);
        }

        const nutrients = app.getAnswer(`amendments/nutrients_${i}`);
        const n = app.getAnswer(`amendments/n_${i}`);
        const p = app.getAnswer(`amendments/p_${i}`);
        const k = app.getAnswer(`amendments/k_${i}`);
        const trace = app.getAnswer(`amendments/nutrients_trace_${i}`);

        const amendment = {
            name,
            type,
            date,
            method,
            nutrients,
            n,
            p,
            k,
            trace
        };
        ret.push(amendment);
    }

    return ret;
}

function weeklyAmendmentParser() {
    const ret = [];
    for (let i = 1; i < 4; i++) {
        const name = app.getAnswer(`amendments/name_${i}`);
        if (!name) {
            continue;
        }

        const type = app.getAnswer(`amendments/amendment_type_${i}`);
        const method = app.getAnswer(`amendments/method_${i}`);

        const nutrients = app.getAnswer(`amendments/nutrients_${i}`);
        const n = app.getAnswer(`amendments/n_${i}`);
        const p = app.getAnswer(`amendments/p_${i}`);
        const k = app.getAnswer(`amendments/k_${i}`);
        const trace = app.getAnswer(`amendments/nutrients_trace_${i}`);

        const amendment = {
            name,
            nutrients,
            type,
            method,
            n,
            p,
            k,
            trace
        };
        ret.push(amendment);
    }

    return ret;
}

/**
 * Returns array of strings representing
 * the labels of the items in Single Select
 * or Multi Select as array
 * @param {Array} questionId
 */
function getItemLabels(questionId, filter) {
    const meta = app.getMeta();
    const answer = app.getAnswer(questionId);
    if (answer === '') {
        return [];
    }
    if (!answer) {
        throw Error(`answer for question not set: ${questionId}`);
    }

    const control = meta.controls.find(
        c => c.name === `/data/${questionId}:label`
    );
    if (!control) {
        throw Error(`unable to find control with ID: ${questionId}`);
    }

    const raw = answer.split(' ');
    const itemIds = filter ? raw.filter(filter) : raw;

    return itemIds.map(id => {
        const r = control.options.find(o => o.value === id);
        if (!r) {
            throw Error(`unable to find label for item ${id} in ${questionId}`);
        }
        return r.text;
    });
}

module.exports = {
    amendmentParser,
    tillageParser,
    weeklyAmendmentParser,
    getItemLabels
};

const app = require('./app');
const formparser = require('./formparser');
const math = require('./math');
const oursci = require('./oursci');
const serial = require('./serial');
const processWavelengths = require('./processWavelengths');
const sendDevice = require('./sendDevice');
// const sheets = require('./sheets');
const soilgrid = require('./soilgrid');
const utils = require('./utils');
const weather = require('./weather');

module.exports = {
    app,
    formparser,
    math,
    oursci,
    serial,
    processWavelengths,
    sendDevice,
    // sheets,
    soilgrid,
    utils,
    weather,
};

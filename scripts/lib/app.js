/* global android */
const papaparse = require('papaparse');
let answers = '';
const DesktopApp = {
    initSubmittedMock: (formId, uuidSnippet) => {
        const syncRequest = require('sync-request');
        const r = syncRequest(
            'GET',
            `https://app.our-sci.net/api/survey/result/csv/by-form-id/${formId}?groupDelimiter=/`
        );

        const parsed = papaparse.parse(r.getBody('utf-8'), {
            header: true
        });
        answers = parsed.data.find(a => a.metaInstanceID.includes(uuidSnippet));
        const measurements = {};
        const keys = Object.keys(answers);
        keys.forEach(k => {
            if (answers[k] === '') {
                answers[k] = undefined;
            }
        });

        keys.forEach(k => {
            const reg = /^(.*?)\.(.*?)\.(.*?)$/g;
            const groups = reg.exec(k);
            if (groups == null) {
                return;
            }

            const question = groups[1];
            const type = groups[2];
            const id = groups[3];

            if (!measurements[question]) {
                measurements[question] = {};
            }

            if (!measurements[question][type]) {
                measurements[question][type] = {};
            }

            measurements[question][type][id] = answers[k];
        });

        Object.keys(measurements).forEach(k => {
            if (!measurements[k].meta.date) {
                answers[k] = null; // behavior of the app, when measurement has not been run
                return;
            }
            answers[k] = JSON.stringify(measurements[k]);
        });

        console.log('using mock answers');
        console.log(JSON.stringify(answers, null, 2));
    },
    result: res => {
        const tmp = JSON.stringify(res, null, 2);
        // DEBUG console.log(`saving result to ./data/result.json:\n${tmp}`);
        const fs = require('fs');
        if (!fs.existsSync('./data')) {
            fs.mkdirSync('./data');
        }
        fs.writeFileSync('./data/result.json', tmp);
        process.exit(0);
    },
    progress: pct => {
        // DEBUG    console.log(`progress is ${pct} %`);
    },
    onSerialDataIntercepted: data => {
        // DEBUG    console.log(`serial data intercepted: ${data}`);
    },
    csvExport: (key, value) => {
        console.log(`exporting key value pair to csv: ${key} => ${value}`);
    },
    csvExportValue: value => {
        console.log(`exporting value to CSV ${value}`);
    },
    getAnswer: dataName => {
        // DEBUG    console.log(`get data name ${dataName}`);
        if (answers) {
            return answers[dataName];
        }
        const fs = require('fs');
        if (fs.existsSync('./mock/answers.json')) {
            const content = fs
                .readFileSync('./mock/answers.json', 'utf-8')
                .toString();
            try {
                return JSON.parse(content)[dataName];
            } catch (error) {
                return undefined;
            }
        }
        return undefined;
    },
    save: () =>
        // DEBUG    console.log('saving env');
        null,
    getEnv: (key, defaultValue = null) => {
        // DEBUG    console.log('getting env var');
        try {
            return window.localStorage.getItem(key);
        } catch (error) {
            // DEBUG      console.error('unable to get value from localstorage');
            return null;
        }
    },

    setEnv: (key, value, description = '') => {
        // DEBUG    console.log('setting env var');
        try {
            window.localStorage.setItem(key, value);
            return true;
        } catch (error) {
            // DEBUG      console.error('unable to set value');
            return false;
        }
    },
    isAndroid: false,
    getCredentials: id => {
        const fs = require('fs');
        const content = fs
            .readFileSync('./mock/credentials.json', 'utf-8')
            .toString();
        try {
            return JSON.parse(content).find(c => `${c.id}` === `${id}`);
        } catch (error) {
            return undefined;
        }
    },
    getMeta: () => {
        const fs = require('fs');
        const content = fs.readFileSync('./mock/meta.json', 'utf-8').toString();
        try {
            return JSON.parse(content);
        } catch (error) {
            return undefined;
        }
    },
    setState: state => {
        console.log(`setting state to: ${state}`);
    },
    error: () => {
        console.log('setting state to error');
    },
    /**
     * Separate multiple addresses with ';'
     */
    email: (to, subject, text, cc) => {
        console.log('send email stub');
    }
};

const AndroidApp = {
    result: res => {
        const tmp = JSON.stringify(res, null, 2);
        android.result(tmp);
    },
    progress: pct => {
        android.progress(pct);
    },
    onSerialDataIntercepted: data => {
        android.onSerialDataIntercepted(data);
    },
    csvExport: (key, value) => {
        android.csvExport(key, value);
    },
    csvExportValue: value => {
        android.csvExport(value);
    },
    getAnswer: dataName => android.getAnswer(dataName),
    save: () => android.save(),
    getEnv: (key, defaultValue = null) => {
        try {
            return android.getEnvVar(key, defaultValue);
        } catch (error) {
            console.error('unable to get value');
            return null;
        }
    },
    setEnv: (key, value, description = '') => {
        try {
            return android.setEnvVar(key, value, description);
        } catch (error) {
            console.error('unable to set value');
            return false;
        }
    },
    /**
     * can throw exception
     */
    getCredentials: id => {
        const r = JSON.parse(android.getCredentials(id));
        if (r.exception) {
            throw r.exception;
        }
        return r;
    },
    getMeta: () => {
        try {
            return JSON.parse(android.getMeta());
        } catch (error) {
            console.error(error);
            return undefined;
        }
    },
    setState: state => {
        android.setState(state);
    },
    error: () => {
        android.setState('error');
    },
    email: (to, subject, text, cc) => {
        try {
            android.email(to, subject, text, cc);
        } catch (error) {
            console.error(error);
        }
    },
    isAndroid: true
};

// export default (() => {
//     if (typeof _ANDROID === 'undefined') {
//         return DesktopApp;
//     }
//     return AndroidApp;
// })();

module.exports = (() => {
    if (typeof _ANDROID === 'undefined') {
        return DesktopApp;
    }
    return AndroidApp;
})();

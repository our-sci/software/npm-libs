const axios = require('axios');


// TODO: add support for location, would need to change function signature to consume object instead of arguments, which would break backwards-compatibility

module.exports = (lat, lon, from, to) => new Promise((resolve, reject) => {
    axios
        // 'http://aesl.ces.uga.edu:3000/averages?lat=39.03&lon=-76.87&start=2018-11-01&end=2018-11-01&output=html'
        .get(`http://aesl.ces.uga.edu:3000/averages?lat=${lat}&lon=${lon}&start=${from}&end=${to}&output=json`)
        .then((response) => {
            console.log(response.data);
            resolve(response.data);
        })
        .catch((error) => {
            console.log(error);
            reject(error);
        });
});

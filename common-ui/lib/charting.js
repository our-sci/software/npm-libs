/* global Plotly */

import * as boxes from './boxes';
import _ from 'lodash';

export function lineChart(title, x, values, axis, wide = false) {
    const t = boxes.genericBoxTemplate({
        supClass: wide ? 'col-lg-12' : 'col-lg-6',
        title
    });

    var div = document.createElement('div');
    div.innerHTML = t;
    div.getElementsByClassName('pad')[0].classList.add('breakout');
    const target = div.getElementsByClassName('target')[0];
    target.classList.add('plotly-box');

    const traces = _.map(values, v => ({
        x,
        y: v,
        type: 'scatter',
        connectgaps: true,
        mode: 'lines',
        line: {
            width: 0.2,
            color: '#3390E6',
            smoothing: 1,
            shape: 'spline',
            simplify: true
        }
    }));

    const layout = {
        xaxis: {
            title: axis[0]
        },
        yaxis: {
            title: axis[1]
        }
    };

    document.getElementById('container').appendChild(div.firstChild);

    Plotly.plot(target, traces, layout, { responsive: true });
}

export function plotly(title, data, layout, config, wide = false, element) {
    if (element) {
        Plotly.purge(element);
        Plotly.plot(element, data, layout, config);
        return element;
    }

    const t = boxes.genericBoxTemplate({
        supClass: wide ? 'col-lg-12' : 'col-lg-6',
        title
    });

    var div = document.createElement('div');
    div.innerHTML = t;
    const target = div.getElementsByClassName('target')[0];
    target.classList.add('plotly-box');
    div.getElementsByClassName('pad')[0].classList.add('breakout');
    document.getElementById('container').appendChild(div.firstChild);

    Plotly.plot(target, data, layout, config);
    return target;
}

export function map(title, pins, wide = false) {
    // map code should probably go here?
}

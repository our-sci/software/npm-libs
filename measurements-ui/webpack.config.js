const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractCSS = new ExtractTextPlugin('styles.min.css');

module.exports = {
	entry: './lib/index.js',
	output: {
		path: __dirname + '/dist',
		filename: 'measurements-ui.js',
		library: '@oursci/measurements-ui',
		libraryTarget: 'umd',
		umdNamedDefine: true,
	},
	module: {
		rules: [{
			test: /\.css$/,
			use: extractCSS.extract(['css-loader']),
		}],
	},
	plugins: [
		extractCSS,
	]
};
const path = require('path');
const webpack = require('webpack');
const fs = require('fs-cli');

const opts = ['android', 'browser'];
const configs = {
  android: {
    externals: [{
      './data/result.json': 'mock',
      serialport: 'SerialPort',
      fs: 'fs',
    }, ],
    filename: 'processor.js',
  },
  browser: {
    externals: {
      serialport: 'SerialPort',
      fs: 'fs',
    },
    filename: 'processor-browser-debug.js',
  }
};


const createPack = (type = 'android') => {
  if (!opts.includes(type)) {
    console.log('usage: node pack-processor (browser|android) defaulting to android');
  }

  const target = {
    filename: configs[type].filename,
    dist: __dirname.includes('node_modules') ? path.resolve(__dirname, '../../../../dist') : path.resolve('./dist'),
    build: __dirname.includes('node_modules') ? path.resolve(__dirname, '../../../../build') : path.resolve('./build'),
  };

  return (done = () => {}) => {
    console.log(configs[type].externals);

    webpack({
      entry: {
        app: ['babel-polyfill', './src/processor.js'],
      },
      output: {
        path: target.build,
        filename: target.filename,
      },
      externals: configs[type].externals,
      module: {
        rules: [{
            test: /\.css$/,
            use: ['style-loader', 'css-loader'],
          },
          {
            test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: ['url-loader?limit=10000&mimetype=application/font-woff'],
          },
          {
            test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: ['file-loader'],
          },
          {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env'],
              },
            },
          },
        ],
      },
    }, (err, stats) => {
      if (err) {
        console.error(err.stack || err);
        if (err.details) {
          console.error(err.details);
          throw err;
        }
        return;
      }

      const info = stats.toJson();

      if (stats.hasErrors()) {
        console.log('\x1b[31m');
        info.errors.forEach((element) => {
          console.error(element);
        });
        console.log('\x1b[0m');
      }

      if (stats.hasWarnings()) {
        console.log('\x1b[33m');
        info.warnings.forEach((element) => {
          console.warn(element);
        });
        console.log('\x1b[0m');
      }

      if (!err && !stats.hasErrors()) {
        fs.mv(path.join(target.build, target.filename), path.join(target.dist, target.filename));
        console.log(`success, processor script: ${path.join(target.dist, target.filename)}`);
        done();

      }
      // Done processing
    });
  };
};

module.exports.pack = createPack();
module.exports.packBrowser = createPack('browser');
module.exports.packAndroid = createPack();
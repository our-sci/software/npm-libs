import * as backend from './oursci-backend';
import * as survey from './survey';

const loadSurvey = backend.loadSurvey;
const surveyUtils = survey;

export { loadSurvey };
export { surveyUtils };

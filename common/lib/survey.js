import Papa from 'papaparse';
import _ from 'lodash';

export function surveyToCSV(survey) {
    const arr = _.values(survey);
    const input = {
        fields: _.keys(survey),
        data: arr.map((col, i) => arr.map(row => row[i]))
    };
    return Papa.unparse(input);
}

export function mergeSurveys(surveysParam) {
    const surveys = surveysParam;

    console.log(_);

    const keys = _.concat([], ..._.map(surveys, survey => _.keys(survey)));
    const uniq = _.uniq(keys);

    _.forEach(uniq, key => {
        _.forEach(surveys, (survey, surveyKey) => {
            const len = survey.metaInstanceID.length;
            if (!_.has(survey, key)) {
                // console.log(`key is missing: ${key} in survey ${surveyKey}`);
                surveys[surveyKey][key] = [...Array(len)];
            }
        });
    });

    const merged = {};

    _.mergeWith(merged, ..._.map(surveys), (objValue, srcValue) => {
        if (_.isArray(objValue)) {
            return objValue.concat(srcValue);
        }
        return undefined;
    });

    return merged;
}

export function joinSurveyOn(
    baseSurveyParam,
    baseSurveyJoinField,
    surveyToJoin,
    surveyToJoinField,
    prefix
) {
    const baseSurvey = baseSurveyParam;
    const addedKeys = _.keys(surveyToJoin).map(k => `${prefix}.${k}`);
    const baseSurveyLen = baseSurvey.metaInstanceID.length;

    function isAlreadyJoined(index) {
        if (baseSurvey[`${prefix}.metaInstanceID`][index]) {
            return true;
        }
        return false;
    }

    _.forEach(addedKeys, k => {
        baseSurvey[k] = [...Array(baseSurveyLen)];
    });

    _.forEach(surveyToJoin[surveyToJoinField], (answer, indexToCopyFrom) => {
        const matches = _.pickBy(
            baseSurvey[baseSurveyJoinField],
            a => answer === a
        );
        _.forEach(matches, (answerValue, indexToCopyTo) => {
            // console.log(`joining answer ${answerValue} and copying to index ${indexToCopyTo}`);

            if (
                _.includes(
                    baseSurvey[`${prefix}.metaInstanceID`],
                    surveyToJoin.metaInstanceID[indexToCopyFrom]
                )
            ) {
                return;
            }

            if (!isAlreadyJoined(indexToCopyTo)) {
                _.forEach(surveyToJoin, (answerArray, question) => {
                    baseSurvey[`${prefix}.${question}`][indexToCopyTo] =
                        surveyToJoin[question][indexToCopyFrom];
                });
            } else {
                _.forEach(_.keys(baseSurvey), k => {
                    baseSurvey[k].push(baseSurvey[k][indexToCopyTo]);
                });
                const len = baseSurvey.metaInstanceID.length;

                _.forEach(surveyToJoin, (answerArray, question) => {
                    baseSurvey[`${prefix}.${question}`][len - 1] =
                        surveyToJoin[question][indexToCopyFrom];
                });
            }
        });
        // console.log(matches);
    });

    return baseSurvey;
}
